#include<stdio.h>
#include<string.h>
#define MIN 0
#define MAX 130
#define START_LETTER_A 65
#define END_LETTER_Z 90
#define START_a 97
#define END_z 122
#define SIZE 20

int validar_nombre(char *);
int validar_edad (int);
void verificar_menor (int *,int, char *, char *, char *, char *);
void verificar_mayor (int *,int, char *, char *, char *, char *);


int main (){
	int es_valido;
	int edad;
	int edad_v;
	int menor_edad=999;
	int mayor_edad=0;
	float edad_promedio=0;
	char mas_viejo_nombre [SIZE];
	char mas_joven_nombre [SIZE];
	char mas_viejo_apellido [SIZE];
	char mas_joven_apellido [SIZE];
	int ap_valido;
	char nombre [SIZE];
	char apellido [SIZE];
	for (int i = 0;i<10;i++){
		do{
			printf("Ingrese nombre de la persona %d: ",i+1);
			scanf("%s",nombre);
			es_valido = validar_nombre(nombre);
		}while(es_valido == 0);
		do{
			printf("Ingrese apellido de la persona %d: ",i+1);
			scanf("%s",apellido);
			ap_valido = validar_nombre(apellido);
		}while(ap_valido == 0);
		do{
			printf("Ingrese edad de la persona %d: ",i+1);
			scanf("%d",&edad);
			edad_v = validar_edad(edad);
		}while(edad_v == 0);
		edad_promedio = edad_promedio + edad;
		verificar_menor(&menor_edad,edad,mas_joven_nombre,mas_joven_apellido,nombre,apellido);
		verificar_mayor(&mayor_edad,edad,mas_viejo_nombre,mas_viejo_apellido,nombre,apellido);
	}
	edad_promedio = edad_promedio/10;
	printf("\nPromedio edades = %f\n",edad_promedio);
	printf("Persona mas vieja: %s, %s,edad = %d\n",mas_viejo_nombre,mas_viejo_apellido,mayor_edad);
	printf("Persona mas joven: %s, %s,edad = %d\n",mas_joven_nombre,mas_joven_apellido,menor_edad);
}


int validar_nombre(char *str){
	for(int i =0;i<SIZE;i++){
		int c = str[i];
		if(i==0){
			if(c<START_LETTER_A || c>END_LETTER_Z){
			return 0;
			}
		}
		else if (c!=0){
			if(c<START_a || c>END_z){
			return 0;
			}
		}
		else{
		break;
		}
	}
	return 1;
}

int validar_edad (int edad){
	if(edad<MIN || edad>MAX){
		return 0;
	}
	else{
		return 1;
	}
}
void verificar_menor (int *menor_edad,int edad, char *mas_joven_nombre, char *mas_joven_apellido, char *nombre, char *apellido){
	if(edad < *menor_edad){
		*menor_edad = edad;
		strcpy(mas_joven_nombre,nombre);
		strcpy(mas_joven_apellido,apellido);
	}
}
void verificar_mayor (int *mayor_edad,int edad, char *mas_viejo_nombre, char *mas_viejo_apellido, char *nombre, char *apellido){
	if(edad > *mayor_edad){
		*mayor_edad = edad;
		strcpy(mas_viejo_nombre,nombre);
		strcpy(mas_viejo_apellido,apellido);
	}
}

